setup:
	pre-commit autoupdate
	pre-commit install

format:
	isort .
	black -v .

lint:
	pre-commit run --all-files
	flake8 .

clean:
	rm -r src/**/__pycache__

test:
	python test.py
