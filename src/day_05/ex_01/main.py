# -*- coding: utf-8 -*-
import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, lambda x: x.replace(" -> ", ",").replace("\n", ""))
    ipt = [[int(z) for z in y.split(",")] for y in ipt]
    max_x = 0
    max_y = 0

    for stream in ipt:
        max_x = max([max_x, stream[0], stream[2]])
        max_y = max([max_y, stream[1], stream[3]])

    field = np.zeros((max_y + 1, max_x + 1))
    for stream in ipt:
        if stream[0] != stream[2] and stream[1] != stream[3]:
            continue

        if stream[0] == stream[2]:
            start = min(stream[1], stream[3])
            stop = max(stream[1], stream[3]) + 1
            field[start:stop, stream[0]] += 1
        else:
            start = min(stream[0], stream[2])
            stop = max(stream[0], stream[2]) + 1
            field[stream[1], start:stop] += 1

    return np.count_nonzero(field > 1)
