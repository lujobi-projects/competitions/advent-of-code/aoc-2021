# -*- coding: utf-8 -*-

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    dist = 0
    depth = 0

    for i in ipt:
        if "forward" in i:
            dist += int(i.replace("forward ", ""))
        elif "down" in i:
            depth += int(i.replace("down ", ""))
        elif "up" in i:
            depth -= int(i.replace("up ", ""))

    return depth * dist
