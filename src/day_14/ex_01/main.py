# -*- coding: utf-8 -*-

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    template = list(ipt[0].replace("\n", ""))

    mapping = {}
    for line in ipt[2:]:
        split = line.replace("\n", "").split(" -> ")
        mapping[split[0]] = split[1]

    for _ in range(10):
        step = [template[0]]
        for (x, y) in zip(template, template[1:]):
            step += [mapping[x + y], y]
        template = step
    counts = {}
    for c in template:
        if counts.get(c):
            counts[c] += 1
        else:
            counts[c] = 1

    return max(counts.values()) - min(counts.values())
