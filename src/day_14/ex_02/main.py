# -*- coding: utf-8 -*-

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    template = list(ipt[0].replace("\n", ""))

    counts = {}
    counts_tmpl = {}
    mapping = {}
    for line in ipt[2:]:
        split = line.replace("\n", "").split(" -> ")
        mapping[split[0]] = split[1]
        counts[split[0][0]] = 0
        counts[split[0][1]] = 0
        counts_tmpl[split[0][0]] = 0
        counts_tmpl[split[0][1]] = 0

    for c in template:
        counts[c] += 1

    def fuse(cts, add_cts):
        for k, v in add_cts.items():
            cts[k] += v
        return cts

    cache = {}

    def eval_pair(x, y, depth):
        name = x + y + str(depth)
        if cache.get(name):
            return cache[name]
        cts = counts_tmpl.copy()
        res_char = mapping[x + y]
        cts[res_char] += 1
        if depth == 1:
            return cts
        cts = fuse(cts, eval_pair(x, res_char, depth - 1))
        cts = fuse(cts, eval_pair(res_char, y, depth - 1))
        if len(cache.keys()) < 10000000 and depth < 30:
            cache[name] = cts
        return cts

    for (x, y) in zip(template, template[1:]):
        counts = fuse(counts, eval_pair(x, y, 40))

    return max(counts.values()) - min(counts.values())
