# -*- coding: utf-8 -*-
import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    ipt = np.array(ipt[0].split(","), dtype=int)
    summary = np.zeros(9)
    for i in range(9):
        summary[i] = np.count_nonzero(ipt == i)

    for _ in range(256):
        summary = np.array([*summary[1:], summary[0]])
        summary[6] += summary[-1]

    return np.sum(summary)
