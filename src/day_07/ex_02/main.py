# -*- coding: utf-8 -*-
import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = np.array(read_ipt(__file__, lambda x: [int(y) for y in x.split(",")])[0])

    min_fuel = np.inf

    for x in range(np.min(ipt), np.max(ipt) + 1):
        dists = np.abs(x * np.ones_like(ipt) - ipt)
        costs = dists * (dists + 1) / 2
        min_fuel = min(min_fuel, np.sum(costs))

    return int(min_fuel)
