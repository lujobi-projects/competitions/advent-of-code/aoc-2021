# -*- coding: utf-8 -*-
from dataclasses import dataclass, field
from typing import List

from src.utils.read_ipt import read_ipt


@dataclass
class Node:
    big: bool = False
    neighbors: List[str] = field(default_factory=list)


def run():
    ipt = read_ipt(__file__, lambda a: a.replace("\n", ""))

    tree = {}

    for line in ipt:
        (s, e) = line.split("-")
        if not tree.get(s):
            tree[s] = Node(s[0].isupper())
        if not tree.get(e):
            tree[e] = Node(e[0].isupper())
        tree[s].neighbors.append(e)
        tree[e].neighbors.append(s)

    def step(path):
        if path[-1] == "end":
            return [path]
        sols = []
        for n in tree[path[-1]].neighbors:
            if tree[n].big or n not in path:
                sols.extend(step([*path, n]))

        return sols

    paths = step(["start"])

    return len(paths)
