# -*- coding: utf-8 -*-
from src.utils.read_ipt import read_ipt

NUMBERS = {
    "abcefg": 0,
    "cf": 1,
    "acdeg": 2,
    "acdfg": 3,
    "bcdf": 4,
    "abdfg": 5,
    "abdefg": 6,
    "acf": 7,
    "abcdefg": 8,
    "abcdfg": 9,
}


def run():
    ipt = read_ipt(
        __file__,
        lambda x: [[z for z in y.split(" ") if z != ""] for y in x.replace("\n", "").split("|")],
    )
    res = 0

    for line in ipt:
        char_ct = {"a": 0, "b": 0, "c": 0, "d": 0, "e": 0, "f": 0, "g": 0}
        mapping = {"a": None, "b": None, "c": None, "d": None, "e": None, "f": None, "g": None}
        one_sign = four_sign = seven_sign = None
        map_f = map_b = map_c = None
        for obs in line[0]:
            if len(obs) == 2:
                one_sign = obs
            elif len(obs) == 3:
                seven_sign = obs
            elif len(obs) == 4:
                four_sign = obs
            for c in obs:
                char_ct[c] += 1
        map_a = list(set(seven_sign).difference(set(one_sign)))[0]
        mapping[map_a] = "a"
        for (k, v) in char_ct.items():
            if v == 6:
                mapping[k] = "b"
                map_b = k
            if v == 4:
                mapping[k] = "e"
            if v == 9:
                map_f = k
                mapping[k] = "f"
        map_c = str(seven_sign).replace(map_a, "").replace(map_f, "")
        mapping[map_c] = "c"
        mapping[str(four_sign).replace(map_b, "").replace(map_c, "").replace(map_f, "")] = "d"
        for k, v in mapping.items():
            if v is None:
                mapping[k] = "g"

        total_no = 0
        for number in line[1]:
            new_no = ""
            for n in number:
                new_no += mapping[n]
            new_no = "".join(sorted(new_no))
            total_no *= 10
            total_no += NUMBERS[new_no]
        res += total_no

    return res
