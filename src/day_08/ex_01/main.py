# -*- coding: utf-8 -*-
from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__, lambda x: [[z for z in y.split(" ") if z != ""] for y in x.replace("\n", "").split("|")])

    ct = 0

    for line in ipt:
        for number in line[1]:
            if len(number) == 2 or len(number) == 3 or len(number) == 4 or len(number) == 7:
                ct += 1

    return ct
