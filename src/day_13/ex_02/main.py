# -*- coding: utf-8 -*-
import logging

import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    folds = []
    dots = []

    for line in ipt:
        if line == "\n":
            continue
        elif "fold along " in line:
            tmp_line = line.replace("fold along ", "").replace("\n", "")
            folds.append(tmp_line.split("="))
        else:
            dots.append([int(x) for x in line.replace("\n", "").split(",")])

    dots = np.array(dots)

    for fold in folds:
        fold_pos = int(fold[1])

        if fold[0] == "x":
            dots[dots > [fold_pos, np.inf]] -= 2 * (dots[dots > [fold_pos, np.inf]] - fold_pos)
            dots[dots >= [fold_pos, np.inf]] = 0
        else:
            dots[dots > [np.inf, fold_pos]] -= 2 * (dots[dots > [np.inf, fold_pos]] - fold_pos)
            dots[dots >= [np.inf, fold_pos]] = 0

    x_max = max(dots.T[0]) + 1
    y_max = max(dots.T[1]) + 1
    paper = np.zeros((y_max, x_max), dtype=int)
    for d in dots:
        paper[d[1], d[0]] = 1

    for x in paper:
        logging.info("".join(["#" if y == 1 else "." for y in x]))

    return "UFRZKAUZ"
