# -*- coding: utf-8 -*-

import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)

    folds = []
    dots = []

    for line in ipt:
        if line == "\n":
            continue
        elif "fold along " in line:
            tmp_line = line.replace("fold along ", "").replace("\n", "")
            folds.append(tmp_line.split("="))
        else:
            dots.append([int(x) for x in line.replace("\n", "").split(",")])

    dots = np.array(dots)

    fold = folds[0]
    fold_pos = int(fold[1])

    if fold[0] == "x":
        dots[dots > [fold_pos, np.inf]] -= 2 * (dots[dots > [fold_pos, np.inf]] - fold_pos)
        dots[dots >= [fold_pos, np.inf]] = 0
    else:
        dots[dots > [np.inf, fold_pos]] -= 2 * (dots[dots > [np.inf, fold_pos]] - fold_pos)
        dots[dots >= [np.inf, fold_pos]] = 0

    return len(np.unique(dots, axis=0))
