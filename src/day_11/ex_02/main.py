# -*- coding: utf-8 -*-

import numpy as np
from scipy import signal

from src.utils.read_ipt import read_ipt


def run():
    ipt = np.array(read_ipt(__file__, lambda a: [int(c) for c in a.replace("\n", "")]))

    ct = 0
    while np.count_nonzero(ipt) != 0:
        ipt += 1
        mask = np.ones_like(ipt)
        while True:
            x = signal.convolve2d(np.logical_and(ipt > 9, mask), np.ones((3, 3)), mode="same")
            mask[ipt > 9] = False
            ipt = np.add(ipt, x)
            if np.count_nonzero(np.logical_and(ipt > 9, mask)) == 0:
                break
        ipt[ipt > 9] = 0
        ct += 1

    return ct
