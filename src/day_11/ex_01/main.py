# -*- coding: utf-8 -*-


import numpy as np
from scipy import signal

from src.utils.read_ipt import read_ipt


def run():
    ipt = np.array(read_ipt(__file__, lambda a: [int(c) for c in a.replace("\n", "")]))
    flashes = 0
    for _ in range(100):
        ipt += 1  # noqa
        mask = np.ones_like(ipt)
        while True:
            x = signal.convolve2d(np.logical_and(ipt > 9, mask), np.ones((3, 3)), mode="same")
            mask[ipt > 9] = False
            ipt = np.add(ipt, x)
            if np.count_nonzero(np.logical_and(ipt > 9, mask)) == 0:
                break
        flashes += np.count_nonzero(ipt > 9)
        ipt[ipt > 9] = 0

    return flashes
