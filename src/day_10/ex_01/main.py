# -*- coding: utf-8 -*-
import logging

from src.utils.read_ipt import read_ipt


def parse_chunk(line, sign):
    line, succ, err = parse(line[1:])
    if not succ:
        return line, False, err
    if len(line) == 0:
        return line, False, "EOF"
    if line[0] != sign:
        return line, False, [line[0], sign]
    return line, True, ""


def parse(line):
    if line is None or len(line) == 0:
        return "", True, ""

    succ = err = None
    while line and line[0] in ["(", "[", "<", "{"]:
        if line[0] == "(":
            line, succ, err = parse_chunk(line, ")")
        elif line[0] == "[":
            line, succ, err = parse_chunk(line, "]")
        elif line[0] == "{":
            line, succ, err = parse_chunk(line, "}")
        elif line[0] == "<":
            line, succ, err = parse_chunk(line, ">")
        if not succ:
            return line, succ, err
        line = line[1:]

    return line, True, ""


COSTS = {")": 3, "]": 57, "}": 1197, ">": 25137}


def run():
    ipt = read_ipt(__file__, lambda a: a.replace("\n", ""))
    cost = 0

    for line in ipt:
        _, succ, err = parse(line)
        if not succ and err != "EOF":
            logging.info(f"{line}: Expected {err[1]}, but found {err[0]} instead. ")
            cost += COSTS[err[0]]

    return cost
