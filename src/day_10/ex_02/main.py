# -*- coding: utf-8 -*-
import logging

import numpy as np

from src.utils.read_ipt import read_ipt


def parse_chunk(line, sign):
    line, succ, err, missing = parse(line[1:])
    if not succ:
        return line, False, err, [*missing, sign]
    if len(line) == 0:
        return line, False, "EOF", [*missing, sign]
    if line[0] != sign:
        return line, False, [line[0], sign], [*missing, sign]
    return line, True, "", missing


def parse(line):
    if line is None or len(line) == 0:
        return "", True, "", []

    succ = err = None
    missing = []
    while line and line[0] in ["(", "[", "<", "{"]:
        if line[0] == "(":
            line, succ, err, missing = parse_chunk(line, ")")
        elif line[0] == "[":
            line, succ, err, missing = parse_chunk(line, "]")
        elif line[0] == "{":
            line, succ, err, missing = parse_chunk(line, "}")
        elif line[0] == "<":
            line, succ, err, missing = parse_chunk(line, ">")
        if not succ:
            return line, succ, err, missing
        line = line[1:]

    return line, True, "", missing


COSTS = {")": 1, "]": 2, "}": 3, ">": 4}


def run():
    ipt = read_ipt(__file__, lambda a: a.replace("\n", ""))
    cost = []

    for line in ipt:
        _, _, err, missing = parse(line)
        if err == "EOF":
            logging.info(f"Missing {''.join(missing)}")

            line_cost = 0
            for m in missing:
                line_cost *= 5
                line_cost += COSTS[m]
            print(line_cost)
            cost += [line_cost]

    print(cost)
    return int(np.median(cost))
