# -*- coding: utf-8 -*-
import numpy as np

from src.utils.read_ipt import read_ipt


def run():
    ipt = read_ipt(__file__)
    input_order = [int(i) for i in ipt[0].split(",")]

    boards = []
    for i in range(2, len(ipt), 6):
        board = []
        for j in range(5):
            board.append([int(row) for row in ipt[i + j].replace("  ", " ").split(" ") if row != ""])
        boards.append(np.array(board))

    win_mask = np.array([-1, -1, -1, -1, -1])
    score = np.inf
    min_idx = 0

    for board in boards:
        for ind, ipt in enumerate(input_order):
            board[board == ipt] = -1
            bingo = any(np.equal(board, win_mask).all(1)) or any(np.equal(board, win_mask).all(0))
            if bingo and ind > min_idx:
                score = np.sum(board[board != -1]) * ipt
                min_idx = ind
                break
            elif bingo:
                break
    return score
