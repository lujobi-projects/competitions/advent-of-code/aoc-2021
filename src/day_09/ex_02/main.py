# -*- coding: utf-8 -*-

import numpy as np
from scipy.ndimage import label
from scipy.signal import argrelmin

from src.utils.read_ipt import read_ipt


def run():
    ipt_orig = np.array(read_ipt(__file__, lambda a: [int(c) for c in a.replace("\n", "")]))
    ipt = np.pad(ipt_orig, 1, constant_values=10)
    x_min = argrelmin(ipt, axis=1)
    x_min = set(zip(x_min[0], x_min[1]))
    y_min = argrelmin(ipt, axis=0)
    y_min = set(zip(y_min[0], y_min[1]))

    mins = y_min.intersection(x_min)

    labels, _ = label(np.logical_not(np.logical_or(ipt == 9, ipt == 10)))

    basins = map(lambda x: np.count_nonzero(labels == labels[x[0], x[1]]), mins)
    return np.product(sorted(basins)[-3:])
