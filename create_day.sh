
if ! [ "$1" -eq "$1" ] 2> /dev/null
then
    echo "argument 1 must be a number"
    return
fi

dir="day_$(printf %02d $1)"

mkdir "src/$dir"
mkdir "src/$dir/ex_01"
touch "src/$dir/ex_01/input.txt"
cp template.py "src/$dir/ex_01/main.py"

mkdir "src/$dir/ex_02"
ln "src/$dir/ex_01/input.txt" "src/$dir/ex_02/input.txt"
cp template.py "src/$dir/ex_02/main.py"
